<?php

require('animal.php');
require('Ape.php');
require('Frog.php');

$sheep = new Animal("shaun");
echo $sheep->name . "<br>";
echo $sheep->legs . "<br>";
echo $sheep->cold_blooded . "<br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>";
$sungokong->yell();
echo "<br>";
$kodok = new Frog("buduk");
echo $kodok->name . "<br>";
$kodok->jump();
